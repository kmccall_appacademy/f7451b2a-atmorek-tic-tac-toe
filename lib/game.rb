require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_reader :player_one, :player_two, :board
  attr_accessor :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_one.mark = :X
    @player_two = player_two
    @player_two.mark = :O
    @board = Board.new
    @current_player = player_one
  end

  def play_turn
    pos = current_player.get_move
    mark = current_player.mark
    board.place_mark(pos, mark)
    switch_players!
  end

  def switch_players!
    @current_player = current_player == player_one ? player_two : player_one
  end

  def play
    until board.over?
      current_player.display(board)
      play_turn
    end
    conclude
  end

  def conclude
    puts "Congratulations, #{board.winner}!!!"
    board.display
  end

end

if __FILE__ == $PROGRAM_NAME
  player_one = HumanPlayer.new("Kyle")
  player_two = ComputerPlayer.new("Computer")
  game = Game.new(player_one, player_two)
  game.play
end
