class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    print "Where would you like to place? (i.e. 0, 2) : "
    move = gets.chomp
    move.split(", ").map(&:to_i)
  end

  def display(board)
    board.display
  end

end

if __FILE__ == $PROGRAM_NAME
end
