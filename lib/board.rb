class Board

  attr_reader :grid, :winning_combos

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
    @winning_combos = [
      [[0, 0], [0, 1], [0, 2]],
      [[1, 0], [1, 1], [1, 2]],
      [[2, 0], [2, 1], [2, 2]],
      [[0, 0], [1, 0], [2, 0]],
      [[0, 1], [1, 1], [2, 1]],
      [[0, 2], [1, 2], [2, 2]],
      [[0, 0], [1, 1], [2, 2]],
      [[2, 0], [1, 1], [0, 2]]
    ]
  end

  def place_mark(pos, mark)
    raise "That space isn't free" if !empty?(pos)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def full?
    grid.none? { |row| row.include?(nil) }
  end

  def winner
    @winning_combos.each do |combo|
      if combo.all? { |pos| self[pos] == :X }
        return :X
      elsif combo.all? { |pos| self[pos] == :O }
        return :O
      end
    end

    nil
  end

  def over?
    return true if !winner.nil?
    full?
  end

  def display
    puts " #{self[[0, 0]]} | #{self[[0, 1]]} | #{self[[0, 2]]}"
    puts "--------"
    puts " #{self[[1, 0]]} | #{self[[1, 1]]} | #{self[[1, 2]]}"
    puts "--------"
    puts " #{self[[2, 0]]} | #{self[[2, 1]]} | #{self[[2, 2]]}"
  end

  def moves_left
    moves_left = []

    (0..2).each do |row|
      (0..2).each do |col|
        pos = [row, col]
        moves_left << pos if self.empty?(pos)
      end
    end

    moves_left
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    grid[row][col] = mark
  end

end

if __FILE__ == $PROGRAM_NAME
end
