class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    # return winning move if available
    board.winning_combos.each do |combo|
      sequence = combo.map { |pos| board[pos] }

      if sequence.count(mark) == 2 && sequence.count(nil) == 1
        win_move = sequence.index(nil)
        return combo[win_move]
      end
    end
    # otherwise, pick random move of moves left
    moves_left = board.moves_left
    moves_left.sample
  end

  def display(board)
    @board = board
  end

end
